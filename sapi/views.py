from django.shortcuts import render
from .models import Peternakan
from django.views.generic import DetailView, CreateView
from django.views.generic import UpdateView
from django.views.generic import DeleteView
from django.views.generic import View
from django.urls import reverse_lazy
from .utils import Render

# Create your views here.
var = {
    'judul' : 'Pendataan Sapi Ternak Se-Kab. Kediri',
     'info' : '''Data sapi ternak yang terdaftar dari seluruh peternakan yang
                berada di kawasan Kab. Kediri''',
     'oleh' : 'owner'
}
def index(self):
    var['peternakan'] = Peternakan.objects.values('id','nama_sapi','kategori').\
         order_by('nama_sapi')
    return render(self, 'sapi/index.html',context=var)

class TernakDetailView(DetailView):
    model = Peternakan
    template_name = 'sapi/sapi_detail_view.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class TernakCreateView(CreateView):
    model = Peternakan
    fields = '__all__'
    template_name = 'sapi/sapi_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class TernakEditView(UpdateView):
    model = Peternakan
    fields = ['nama_sapi','kategori','keterangan',
              'berat','harga','jumlah']
    template_name = 'sapi/sapi_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class TernakDeleteView(DeleteView):
    model = Peternakan
    template_name = 'sapi/sapi_delete.html'
    success_url = reverse_lazy('home_page')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class TernakToPdf(View):
    def get(self,request):
        var = {
            'peternakan' : Peternakan.objects.values(
                'nama_sapi','kategori','harga','keterangan'),
            'request':request
        }
        return Render.to_pdf(self,'sapi/sapi_to_pdf.html',var)
