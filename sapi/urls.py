from django.urls import path
from .views import index, TernakDetailView, TernakCreateView
from .views import TernakEditView, TernakDeleteView, TernakToPdf

urlpatterns = [
    path('', index, name='home_page'),
    path('peternakan/<int:pk>', TernakDetailView.as_view(),
         name='sapi_detail_view'),
    path('peternakan/add', TernakCreateView.as_view(), name='sapi_add'),
    path('peternakan/edit/<int:pk>', TernakEditView.as_view(), name='sapi_edit'),
    path('peternakan/delete/<int:pk>', TernakDeleteView.as_view(),
         name='sapi_delete'),
    path('peternakan/print_pdf', TernakToPdf.as_view(),name='sapi_to_pdf')
]
